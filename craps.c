/**
 * Game of luck: Implementation of the Gamemaster
 *
 * Course: Operating Systems and Multicore Programming - OSM lab
 * assignment 1: game of luck.
 *
 * Author: Nikos Nikoleris <nikos.nikoleris@it.uu.se>
 *
 */

#include <stdio.h> /* I/O functions: printf() ... */
#include <stdlib.h> /* rand(), srand() */
#include <unistd.h> /* read(), write() calls */
#include <assert.h> /* assert() */
#include <time.h>	/* time() */
#include <signal.h> /* kill(), raise() and SIG???? */

#include <sys/types.h> /* pid */
#include <sys/wait.h> /* waitpid() */

#include "common.h"

#define close_or_fail(fd)						\
	if (close(fd) == -1) {						\
		fprintf(stderr, "(%d)%d, Closing fd %d failed\n",	\
			getpid(),					\
			__LINE__,					\
			fd);						\
		exit(EXIT_FAILURE);					\
	}

int main(int argc, char *argv[])
{
	/* Remove unused variable warning */
	(void)argc;
	(void)argv;

	int i, j;
	const char *randomFile = "/dev/urandom";

	/* DONE: Use the following variables in the exec system call. Using the
	 * function sprintf and the arg1 variable you can pass the id parameter
	 * to the children
	 */

	char arg0[] = "./shooter";
	char arg1[10];
	char *args[] = {arg0, arg1, NULL};

	int pipe_score[NUM_PLAYERS][2];
	int pipe_seed[NUM_PLAYERS][2];
	pid_t children[NUM_PLAYERS];
	/* DONE: initialize the communication with the players */
	for (i = 0; i < NUM_PLAYERS; i++) {

		pipe(pipe_score[i]);
		pipe(pipe_seed[i]);
	}

	for (i = 0; i < NUM_PLAYERS; i++) {
		switch (children[i] = fork()) {
		case -1:
			/* The fork failed */
			fprintf (stderr, "fork() returned a error!\n");
			exit(EXIT_FAILURE);
			break;
		case 0:
			/* This is the child */

			/* Close all unnecessary file descriptors  */
			for (j = i; j < NUM_PLAYERS; j++) {
				if (j != i){
					close_or_fail(pipe_score[j][1]);
					close_or_fail(pipe_score[j][0]);
					close_or_fail(pipe_seed[j][0]);
					close_or_fail(pipe_seed[j][1]);
				}
			}
			close_or_fail(pipe_seed[i][1]);
			close_or_fail(pipe_score[i][0]);

			/* Make stdin the read end of the "seed" pipe */
			dup2(pipe_seed[i][0], STDIN_FILENO);
			/* Make stdout the write end of the "score" pipe */
			dup2(pipe_score[i][1], STDOUT_FILENO);
			close_or_fail(pipe_seed[i][0]);
			close_or_fail(pipe_score[i][1]);
			/* The second argument should be the child id */
			sprintf(arg1, "%d", i);

			/* Run shooter */
			execv(args[0], args);

			/* We chould never get here */
			fprintf (stderr, "Exec should not return!\n");
			exit(EXIT_FAILURE);
			break;
		default:
			/* This is the parent */
			close_or_fail(pipe_seed[i][0]);
			close_or_fail(pipe_score[i][1]);
			break;
		}


	}

	int seed;
	/* Check if the randomFile exists */
	if (access( randomFile, R_OK ) != -1 ){
		FILE *fp = fopen(randomFile, "r");
		fread(&seed, sizeof(int), 1, fp);
		fclose(fp);
	} else {
		/* No randomfile exists, seed with time() */
		seed = time(NULL);
	}
	for (i = 0; i < NUM_PLAYERS; i++) {
		seed++;
		/* DONE: send the seed to the players */
		write(pipe_seed[i][1], &seed, sizeof(int));
		close_or_fail(pipe_seed[i][1]);
	}


	int winner_score = 0;
	int score = 0;
	/* DONE: get the dice results from the players, find the winner */
	for (i = 0; i < NUM_PLAYERS; i++) {
		read(pipe_score[i][0], &score, sizeof(int));
		close_or_fail(pipe_score[i][0]);
		if(score > winner_score){
			winner_score = score;
			winner = i;
		}
	}
	printf("master: player %d WINS\n", winner);

	/* DONE: signal the winner */
	kill(children[winner], SIGUSR1);

	/* DONE: signal all players the end of game */
	for (i = 0; i < NUM_PLAYERS; i++) {
		kill(children[i], SIGUSR2);
	}

	printf("master: the game ends\n");

	/* DONE: cleanup resources and exit with success */

	pid_t child_pid;
	int chlid_exit_status;
	for (i = 0; i < NUM_PLAYERS; i++) {
	        child_pid = wait(&chlid_exit_status);
		waitstat(child_pid, chlid_exit_status);
	}
	return 0;
}
